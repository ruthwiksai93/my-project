package challenge;

public class Method6 {

	public static void main(String[] args) {

		int highscoreposition;
		

		highscoreposition = calculatehighscoreposition(1500);
		displayhighscoreposition("TIN", highscoreposition);

		highscoreposition = calculatehighscoreposition(900);
		displayhighscoreposition("bob", highscoreposition);

		highscoreposition = calculatehighscoreposition(400);
		displayhighscoreposition("percy", highscoreposition);

		highscoreposition = calculatehighscoreposition(50);
		displayhighscoreposition("gilbert", highscoreposition);

	}

	public static void displayhighscoreposition(String playerName, int position) {
		System.out.println(playerName + " managed to get into position " + position + "on the highscore table");

	}
	

	public static int calculatehighscoreposition(int playerscore) {
		if (playerscore > 1000) {
			return 1;
		} else if (playerscore > 500 && playerscore < 1000) {
			return 2;
		} else if (playerscore > 100 && playerscore < 500) {
			return 3;
		} else {
			return 4;
		}
	}
}
